import { configureStore } from '@reduxjs/toolkit'
import cardsReducer from './Routes/AddCard/reducer';
import bannedCountriesReducer from './Routes/Admin/reducer';

export const store = configureStore({
  reducer: {
    cards: cardsReducer,
    bannedCountries: bannedCountriesReducer,
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch