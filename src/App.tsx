import React from 'react';
import { Toaster } from 'react-hot-toast';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './Components/Header';
import AddCard from './Routes/AddCard';
import Admin from './Routes/Admin';

const App = () => {
  return (
    <div className="App" >
      <Router>
        <Header />
        <Toaster />
        <Routes>
          <Route path="/" element={<AddCard />}/>
          <Route path="/admin" element={<Admin />}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
