import { CountryType } from './../../Utils/constants';

export interface IAdminState {
    bannedCountries: CountryType[],
}

export const initialState: IAdminState = {
    bannedCountries: [],
};