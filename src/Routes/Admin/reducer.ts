import { CountryType } from './../../Utils/constants';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { initialState } from './constants';

const adminSlice = createSlice({
  name: 'admin',
  initialState,
  reducers: {
    addCountry: (state, action: PayloadAction<CountryType>) => {
        state.bannedCountries = [...state.bannedCountries, action.payload];
    },
    removeCountry: (state, action: PayloadAction<CountryType>) => {
        const countryIndex = state.bannedCountries.findIndex((country) => country.code === action.payload.code);
        state.bannedCountries.splice(countryIndex, 1);
    },
  },
});
export const { addCountry, removeCountry } = adminSlice.actions;
export default adminSlice.reducer;