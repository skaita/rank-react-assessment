import React, { SyntheticEvent } from "react";
import { Autocomplete, TextField } from "@mui/material";
import { difference } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { CountryCodes, CountryType } from "../../Utils/constants";
import { ICard } from "../AddCard/constants";
import { addCountry, removeCountry } from "./reducer";

import "./style.scss";

const Admin = () => {
    const dispatch = useDispatch();
    const bannedCountries: CountryType[] = useSelector((state: RootState) => state.bannedCountries.bannedCountries);
    const existingCards: ICard[] = useSelector((state: RootState) => state.cards.cards);
    const onCountryChange = (event: SyntheticEvent, value: CountryType[]) => {
        if (value.length > bannedCountries.length) {
            const countryToAdd = difference(value, bannedCountries)[0];
            dispatch(addCountry(countryToAdd));
        }
        if (value.length < bannedCountries.length) {
            const countryToRemove = difference(bannedCountries, value)[0];
            dispatch(removeCountry(countryToRemove));
        }
    };
    return (
        <div className="admin">
            <h1 className="admin__title">Admin</h1>
            <div className="admin__banned-countries-select" >
                <Autocomplete
                    multiple
                    id="tags-standard"
                    options={CountryCodes}
                    getOptionLabel={(option) => option.label}
                    onChange={onCountryChange}
                    value={bannedCountries}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            label="Add Banned Countries"
                            placeholder="Search country"
                        />
                    )}
                />
            </div>
            <div className="admin__existing-cards" >
                <h3 className="admin__title admin__title--subsection">Exisiting Cards</h3>
                <div className="admin__cards-list" >
                    {existingCards.map((card) => (
                        <div key={card.cardNumber} className="admin__card-item">
                            <h4>{card.cardNumber}</h4>
                            <h6>{card.cardHolder}</h6>
                            <p>{card.expiryDate}</p>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Admin;