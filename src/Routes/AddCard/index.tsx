import React, { ChangeEvent, FormEventHandler, SyntheticEvent, useState } from "react";
import { Autocomplete, Button, TextField } from "@mui/material";
import { Box } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { CountryCodes, CountryType } from "../../Utils/constants";
import { FieldNames, ICard } from "./constants";
import { addCard } from "./reducer";
import toast from "react-hot-toast";

import "./style.scss";

const AddCard = () => {
    const dispatch = useDispatch();
    const [cardState, setCardState] = useState<ICard>({
        [FieldNames.cardNumber]: '',
        [FieldNames.cardHolder]: '',
        [FieldNames.expiryDate]: '',
        [FieldNames.cvv]: '',
        [FieldNames.country]: {
            code: 'ZA',
            label: 'South Africa',
            phone: '27'
        },
    });
    const { cardNumber, cardHolder, cvv, expiryDate, country } = cardState;
    const bannedCountries: CountryType[] = useSelector((state: RootState) => state.bannedCountries.bannedCountries);
    const existingCards: ICard[] = useSelector((state: RootState) => state.cards.cards);
    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        setCardState((currState) => ({
            ...currState,
            [name]: value,
        }));
    };
    const sanitizeInput: FormEventHandler = (event: ChangeEvent<HTMLInputElement>) => {
        const field = event.target;
        const { name, value } = field;
        switch (name) {
            case FieldNames.cardNumber:
                field.value = value.replaceAll(new RegExp(/[^\d]/, 'g'), '').substring(0, 16);
                break;
            case FieldNames.expiryDate:
                field.value = value.replaceAll(new RegExp(/[^\d|/]/, 'g'), '').substring(0, 5);
                break;
            case FieldNames.cvv:
                field.value = value.replaceAll(new RegExp(/[^\d]/, 'g'), '').substring(0, 3);
                break;
        }
    };
    const onCountryChange = (event: SyntheticEvent, value: CountryType | null) => {
        setCardState((currState) => ({
            ...currState,
            [FieldNames.country]: value as CountryType,
        }));
    };
    const onSubmit: FormEventHandler = (event) => {
        event.preventDefault();
        const isValidCountry = !(bannedCountries.find((bannedCountry) => bannedCountry.code === country.code));
        const isCardUnique = !(existingCards.find((existingCard) => existingCard.cardNumber === cardState.cardNumber));
        if (!isValidCountry) toast.error(`${country.label} is banned! Please select another country`);
        if (!isCardUnique) toast.error(`Card number: '${cardNumber}' already exists!`);
        if (isValidCountry && isCardUnique) {
            dispatch(addCard(cardState));
            toast.success('Card successfully added');
        }
    };
    return (
        <div className="add-card" >
            <h1 className="add-card__title" >Add Card</h1>
            <form className="add-card__field-container" onSubmit={onSubmit}>
                <TextField
                    name={FieldNames.cardNumber}
                    className="add-card__field"
                    label="Card Number"
                    type="text"
                    value={cardNumber}
                    onChange={onChange}
                    onInput={sanitizeInput}
                    required
                />
                <TextField
                    name={FieldNames.cardHolder}
                    className="add-card__field"
                    label="Cardholder"
                    type="text"
                    value={cardHolder}
                    onChange={onChange}
                    onInput={sanitizeInput}
                    required
                />
                <TextField
                    name={FieldNames.expiryDate}
                    className="add-card__field add-card__field--short"
                    label="Expiry Date"
                    type="text"
                    placeholder="MM/YY"
                    value={expiryDate}
                    onChange={onChange}
                    onInput={sanitizeInput}
                    required
                />
                <TextField
                    name={FieldNames.cvv}
                    className="add-card__field add-card__field--short"
                    label="CVV"
                    type="text"
                    value={cvv}
                    onChange={onChange}
                    onInput={sanitizeInput}
                    required
                />
                <Autocomplete
                    className="add-card__field add-card__field--short"
                    id="country-select-demo"
                    sx={{ width: 300 }}
                    options={CountryCodes}
                    value={country}
                    autoHighlight
                    getOptionLabel={(option) => option.label}
                    onChange={onCountryChange}
                    renderOption={(props, option) => (
                        <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        <img
                            loading="lazy"
                            width="20"
                            src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
                            srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
                            alt=""
                        />
                        {option.label} ({option.code})
                        </Box>
                    )}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label="Choose a country"
                            inputProps={{
                                ...params.inputProps,
                                autoComplete: 'new-password',
                            }}
                        />
                    )}
                />
                <Button className="add-card__submit" variant="contained" type="submit">Save Card</Button>
            </form>
        </div>
    );
};

export default AddCard;