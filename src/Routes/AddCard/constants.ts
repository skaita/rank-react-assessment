import { CountryType } from './../../Utils/constants';

export enum FieldNames {
    cardNumber = 'cardNumber',
    cardHolder = 'cardHolder',
    expiryDate = 'expiryDate',
    cvv = 'cvv',
    country = 'country',
};

export interface ICard {
    [FieldNames.cardNumber]: string,
    [FieldNames.cardHolder]: string,
    [FieldNames.expiryDate]: string,
    [FieldNames.cvv]: string,
    [FieldNames.country]: CountryType,
}

export interface ICardsState {
    cards: ICard[],
}

export const initialState: ICardsState = {
    cards: [],
};