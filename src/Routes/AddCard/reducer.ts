import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { initialState, ICard } from './constants';

const cardSlice = createSlice({
  name: 'cards',
  initialState,
  reducers: {
    addCard: (state, action: PayloadAction<ICard>) => {
        state.cards = [...state.cards, action.payload];
    },
  },
});
export const { addCard } = cardSlice.actions;
export default cardSlice.reducer;