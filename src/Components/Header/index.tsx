import { Button } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import "./style.scss";

const Header = () => {
    return (
        <div className="header" >
            <h2 className="header__brand" >Card App</h2>
            <div className="header__links" >
                <ul>
                    <li>
                        <Link to="/">
                            <Button variant="contained" >Add Card</Button>
                        </Link>
                    </li>
                    <li>
                        <Link to="/admin">
                            <Button variant="contained" >Admin</Button>
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default Header;